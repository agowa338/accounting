// docker run -itu 1000 buildkite/puppeteer
'use strict';

const puppeteer = require('puppeteer');
const fs = require('fs');
const mariadb = require('mariadb');
const pool = mariadb.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  connectionLimit: 5
});
const username = '';
const password = '';

let debug_browser = null;
let debug_page = null;

async function n26_login(page) {
  await page.setJavaScriptEnabled(false);
  // Login
  await page.goto('https://app.n26.com/login', {waitUntil: 'domcontentloaded'});
  await page.waitFor(5000);
  await page.type('input[id="email"]', username);
  await page.type('input[id="password"]', password);
  await page.click('[type="submit"]');
  await page.waitFor(4000);
  // now we're informed about the 2nd factor being required if proceeded
  await page.click('[type="submit"]');
  await page.waitFor(4000);
  // Now 2nd factor validation should be prompted.
  // Therefore we wait 20 seconds to validate it TODO: Add user prompt before continuing.
  await page.waitFor(20000);
  await page.click('[type="submit"]'); // This dialog is only shown if javascript is disabled.
  await page.waitFor(4000);

  // Now we're logged in and are on the https://app.n26.com/transactions page.
  // We'll fetch basic information about transactions.
  await page.setJavaScriptEnabled(true);
  await page.reload();
  await page.waitFor(4000);
};

async function goto_page_until_success(page, link) {
  let pageStatus = (await page.goto(link, {waitUntil: 'domcontentloaded'})).status();
    while (!((200 <= pageStatus) && (pageStatus < 300))) {
      // we ran into a rate limit
      console.log("ran into rate limit, waiting for 10 seconds");
      await page.waitFor(10000);
      pageStatus = (await page.goto(link, {waitUntil: 'domcontentloaded'})).status();
    };
    if ((await page.title()).includes("Login — ")) {
      try {
        await n26_login(page);
      } catch (error) {
        console.log("login failed, retrying...")
        await page.waitFor(10000)
      }
      await goto_page_until_success(page, link);
    };
};

async function query_n26() {
  // Init
  const browser = await puppeteer.launch({
    //headless: true,
    headless: false,
    args: ['--no-sandbox', '--disable-setuid-sandbox', ]
  });
  const page = await browser.newPage();
  debug_browser = browser;
  debug_page = page;
  await goto_page_until_success(page, 'https://app.n26.com/transactions');

  for (;;) {
    // click load more button until it disapears
    let linkHandlers = await page.$x('//*[@d="M0 8.77a1.53 1.53 0 0 1 2.61-1.09L16 21.07 29.39 7.68a1.53 1.53 0 0 1 2.16 2.16L17.08 24.32a1.53 1.53 0 0 1-2.16 0L.45 9.85A1.53 1.53 0 0 1 0 8.77z"]/../..');
    if (linkHandlers.length > 0) {
      //await page.evaluate(() => { window.scrollTo(0, window.document.body.scrollHeight); });
      await page.evaluate(() => { document.documentElement.scrollTop = ((document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight); });
      await page.waitFor(2000);
      await linkHandlers[0].click();
      console.log("overview: load more transactions")
      await page.waitFor(5000);
    } else {
      console.log("overview: all transactions loaded")
      await page.waitFor(5000);
      break;
    }
  };
  let pageStateVars = await page.evaluate(() => {
    let overviewTransactions = Array.prototype.filter.call(document.getElementsByTagName("a"), x => x.href.includes("/transactions/"));
    overviewTransactions = overviewTransactions.filter(x => !x.href.includes("search"))
    return overviewTransactions.map(x => {
      return ({
        Title: x.text,
        Datetime: x.parentElement.getElementsByTagName("time")[0].dateTime,
        Type: Array.from(x.parentElement.parentElement.getElementsByTagName("span")).slice(-2)[0].innerText,
        Value: Array.from(x.parentElement.parentElement.getElementsByTagName("span")).slice(-1)[0].innerText,
        Link: x.href,
        transactionID: x.href.replace(".*/", ""),
        Message: null,
        IBAN: null,
        BIC: null,
        Category: null,
        Tags: []
      });
    });
  });
  // Fetch IBAN, BIC and Tags for each transaction.
  // because of rate limiting we can not do it concurrently.
  for (let i = 0; i < pageStateVars.length; i++) {
    console.log("Currently processing: " + i + "/" + (pageStateVars.length -1));
    let link = pageStateVars[i]["Link"];
    await goto_page_until_success(page, link);
    // prevent page idle
    page.mouse.move(100,100);
    page.mouse.move(200,200);

    let parsedTransactionDetails = await page.evaluate(() => {
      let _message = null;
      let _iban = null;
      let _bic = null;
      let _tags = [];
      let _category = null;
      try {
        _message = document.evaluate('//span[contains(.,"Reference message")]/../span[not(contains(.,"Reference message"))]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.innerText;
      } catch {};
      try {
        _iban = document.evaluate('//span[contains(.,"IBAN")]/../span[2]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.innerText;
      } catch {};
      try {
        _bic = document.evaluate('//span[contains(.,"IBAN")]/../span[4]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.innerText;
      } catch {};
      try {
        _tags = Array.prototype.map.call(document.evaluate('//span[contains(.,"Tags")]/../../div[2]/div', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.getElementsByTagName("a"), x => x.childNodes[2].nodeValue);
      } catch {};
      try {
        _category = document.evaluate('//option[@selected]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.innerText;
      } catch {};
      return ({
        Message: _message,
        IBAN: _iban,
        BIC: _bic,
        Tags: _tags,
        Category: _category
      });
    });
    pageStateVars[i]["Message"] = parsedTransactionDetails["Message"];
    pageStateVars[i]["IBAN"] = parsedTransactionDetails["IBAN"];
    pageStateVars[i]["BIC"] = parsedTransactionDetails["BIC"];
    pageStateVars[i]["Tags"] = parsedTransactionDetails["Tags"];
    pageStateVars[i]["Category"] = parsedTransactionDetails["Category"];
  };

  // Write output to n26.json
  fs.writeFile('./n26.json', JSON.stringify(pageStateVars), err => err ? console.log(err): null);
  console.log('all transactions saved to ./n26.json');

  // Logout
  await page.goto('https://app.n26.com/logout');
  await page.waitFor(4000);

  // await page.screenshot({ path: '/tmp/screenshot.png' });
  // const firstPar = await page.$$eval('html body', el => el[0].innerHTML);
  //console.log(transactions);
  await browser.close();

  return pageStateVars;
};

query_n26();
